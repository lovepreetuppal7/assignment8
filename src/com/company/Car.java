package com.company;
import java.awt.*;

/**
 * Car Component draws the shapes using Getters and Setters Below
 *
 */
public class Car {
    private int x;
    private int y;

    public void draw(Graphics2D g2) {
        // Car Chassis Construction
        g2.draw(new Rectangle(x,y,100,20));
        g2.draw(new Rectangle(x+30,y-17,40,17));
        //Point2D.Double r1 = new Point2D.Double(x+10,y+10);
        // Car Tire Construction
        g2.drawOval(x+10,y+20,20,20);
        g2.drawOval(x+70,y+20,20,20);
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public Car(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
