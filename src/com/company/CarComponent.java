package com.company;

import javax.swing.*;
import java.awt.*;

/**
 * Animates the car
 */
public class CarComponent extends JComponent {
    private final Car car;
    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        car.draw((Graphics2D) graphics);
    }
    public CarComponent(){
        car = new Car(10,50);
    }
    /**
     * Moves the car
     */
    public void moveCarBy(int x,int y){
        car.setX(car.getX()+x);
        car.setY(car.getY()+y);
        repaint();
    }

}
