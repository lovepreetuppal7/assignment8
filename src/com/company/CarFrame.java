package com.company;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Animation frame and time settings for the Car component, with the frame using finalized instance variables.
 */
public class CarFrame extends JFrame {
    private static final int FRAME_WIDTH = 400;
    private static final int FRAME_HEIGHT = 250;
    private final CarComponent scene;

    class TimerListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            scene.moveCarBy(2,1);
        }
    }
    public CarFrame(){
        scene = new CarComponent();
        add(scene);
        setSize(FRAME_WIDTH,FRAME_HEIGHT);
        ActionListener listener = new CarFrame.TimerListener();
        final int DELAY = 100;
        Timer t = new Timer(DELAY,listener);
        t.start();
    }
}
