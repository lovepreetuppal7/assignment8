package com.company;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

/**
 * The function counts how many times each of the 2 buttons has been hit
 */
public class Click implements ActionListener{
    private JButton button1, button2;
    private int counter1, counter2;

    public Click(JButton button1, JButton button2) {
        this.button1 = button1;
        this.button2 = button2;
        this.counter1 = this.counter2 = 0;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button1) {
            counter1++;
            System.out.println("Button 1: I was clicked " + counter1 + " times!");
        }
        else {
            counter2++;
            System.out.println("Button 2: I was clicked " + counter2 + " times!");
        }
    }
}
