package com.company;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.*;

/**
 * Button creates a program with 2 buttons and counts how many times each button has been hit
 */
public class E10_18 {
    private static final int FRAME_WIDTH = 200;
    private static final int FRAME_HEIGHT = 200;
    public static void main(String[] args){
        JFrame frame = new JFrame();
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        // Add buttons in the frame
        JButton button1 = new JButton("Button 1: Click me!");
        JButton button2 = new JButton("Button 2: Click me!");
        panel.add(button1);
        panel.add(button2);
        frame.add(panel);

        ActionListener listener = new Click(button1, button2);
        button1.addActionListener(listener);
        button2.addActionListener(listener);

        // Gets the height and width of the frame from private integers
        frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
