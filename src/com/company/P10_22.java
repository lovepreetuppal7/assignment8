package com.company;
import javax.swing.*;

/**
 * The function takes input from java files(car, carFrame, CarComponent) and displays the frame along with car animation
 */
public class P10_22 {
    public static void main(String[] args){
        JFrame frame = new CarFrame();
        frame.setTitle("Animation Viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
