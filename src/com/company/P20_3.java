package com.company;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;


public class P20_3 {
    private static JTextField amountField, interestField, yearsField;
    private static JButton calculateButton;
    private static JTextArea resArea;

    /**
     * This is the main method which is responsible for running program.
     * @param args command line arguments
     * */
    public static void main(String[] args){
        // create a frame for the GUI
        JFrame mainFrame = new JFrame("Balance Calculator");
        // create a main panel
        JPanel mainPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

        /* PANEL FOR THE INITIAL AMOUNT LABEL AND TEXT FIELD */
        GridLayout grid = new GridLayout(1, 2);
        // set a horizontal gap for the grid
        grid.setHgap(5);
        JPanel amouPanel = new JPanel(grid);
        JLabel amountLabel = new JLabel("Initial amount: ");
        amountField = new JTextField(10);
        // add the label and the text field to the amouPanel
        amouPanel.add(amountLabel);
        amouPanel.add(amountField);

        /* PANEL FOR THE ANNUAL INTEREST LABEL AND TEXT FIELD */
        JPanel interestPanel = new JPanel(grid);
        JLabel interestLabel = new JLabel("Annual interest(%): ");
        interestField = new JTextField(10);
        // add the label and the text field to the interestPanel
        interestPanel.add(interestLabel);
        interestPanel.add(interestField);

        /* PANEL FOR THE YEARS LABEL AND TEXT FIELD */
        JPanel yearsPanel = new JPanel(grid);
        JLabel yearsLabel = new JLabel("Years: ");
        yearsField = new JTextField(10);
        // add the label and the text field to the yearsPanel
        yearsPanel.add(yearsLabel);
        yearsPanel.add(yearsField);

        /* PANEL FOR THE CALCULATE BUTTON */
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        calculateButton = new JButton("Calculate");
        buttonPanel.add(calculateButton);
        JPanel resPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        resArea = new JTextArea(10, 30);
        resArea.setEditable(false);
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        resArea.setBorder(border);
        resPanel.add(resArea);


        mainPanel.add(amouPanel);
        mainPanel.add(interestPanel);
        mainPanel.add(yearsPanel);
        mainPanel.add(buttonPanel);
        mainPanel.add(resPanel);
        mainFrame.add(mainPanel);
        mainFrame.setSize(300, 300);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);


        /**
         *  CALCULATE BUTTON USING LAMBDA EXPRESSION
         * The function contains the formula for calculation of interests after x years
         * */
        calculateButton.addActionListener(event -> {
            // first check if data in all the fields are provided
            if(amountField.getText().equals("") || interestField.getText().equals("")
                    || yearsField.getText().equals("")) {
                JOptionPane.showMessageDialog(null, "All fields are mandatory!");
                return;
            }
            // now that all data has been provided, parse them to doubles and integers
            double accountBalance = Double.parseDouble(amountField.getText().trim());
            double interestRate = Double.parseDouble(interestField.getText().trim());
            int years = Integer.parseInt(yearsField.getText().trim());

            String result = "Initial Balance: $" + String.format("%.2f", accountBalance)+ "\n";
            for(int i = 0; i < years; i++) {
                double simpleInterest = (accountBalance * interestRate)/100;
                accountBalance += simpleInterest;
                result += "Balance after year " + (i + 1) + ": $" + String.format("%.2f", accountBalance) + "\n";
            }
            resArea.setText(result);
        });
    }
}
